# StanPiggy App
Welcome to StanPiggy, a financial management prototype app, built to take part in the Stanbic Bank App Challenge.

“I want to be your financial friend. Just tell me all your financial transactions everyday, and I will assist you with insights each month, that will help you know if you're going overboard with your expenditure, or how much you're saving each month.

“I offer insightful reminders and summaries periodically, to help you take control of your financial life.

“Count me in! My name is StandPiggy.”

# Screenshots
Below are some screenshots of the prototype

# The technologies
 - HTML5, CSS3 and Javascript
 - The Ionic Framework 2 in concert with
 - AngularJS Framework 2 in union with
 - Many more creative projects and scripts

## Why?
 - Future-proof technologies, ensuring faster, efficient and cost effective app development. The future is now
 - Use of HTML5, CSS and Javascript open limitless possibilities of writing few lines of code but run cross-platform, namely Windows, Android or iOS
 - The use of common technologies that are easy-to-use, means whoever picks up codebase can hit ground running, fast!

# Primary Features
 - *Transactions:* Create transactions when you create them, and see breakdowns of your income and expenditure each month, or search time ranges of how you do.
 - *Synchronized, always:* What you add on your phone is on your PC or tablet. Automatically! No strings attached. Start on any device and finish 'else-device'
 - *Family:* Invite family members to join your StanPiggy board, and as a family work towards a common financial target. See what incomes and expenditure each member makes.
 - *Insights:* Gain insights into how your financials stand over period of times i.e, over a year or even five years. StanPiggy gives you the opportunity to learn what your expenditure and income trends are which are vital in shaping how your future spending should or can be.

# How to run?
 - Follow the installation guide on Ionic Framework. Install Ionic and have your environment set properly. This app uses the latest of Ionic 2, at the time of writing, which is Ionic 2 Beta 7.
 - Clone this repository
 - Change directory to project root
 - Run `ionic serve` to view in browser
 - Run `ionic build android` to build an android APK.