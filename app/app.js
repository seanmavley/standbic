import { App, Platform, IonicApp, MenuController, Modal, Storage, LocalStorage } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { ViewChild } from '@angular/core';
import { TabsPage } from './pages/tabs/tabs';
import { SettingsPage } from './pages/settings/settings';
import { ProfilePage } from './pages/profile/profile';
import { CategoryPage } from './pages/category/category';
import { AddcategoryPage } from './pages/addcategory/addcategory';
import { AboutPage } from './pages/about/about';
import { OverviewPage } from './pages/overview/overview';
import { InsightsPage } from './pages/insights/insights';
import { FeedbackModal } from './pages/feedback/feedback';
import { TutorialPage } from './pages/tutorial/tutorial';

// Services
import { CategoryService } from './providers/category-service/category-service';
import { TransactionService } from './providers/transaction-service/transaction-service';

@App({
    templateUrl: 'build/app.html',
    config: {},
    providers:[CategoryService, TransactionService],
    queries: {
        nav: new ViewChild('content')
    }
})
export class MyApp {
    static get parameters() {
        return [
            [IonicApp],
            [Platform],
            [MenuController],
        ];
    }

    constructor(app, platform, menu) {
        this.app = app;
        this.platform = platform;
        this.menu = menu;
        this.initializeApp();
        
        // pages in side menu
        this.pages = [
            { title: 'Transactions', component: TabsPage, icon: 'briefcase'},
            { title: 'Category', component: CategoryPage, icon: 'albums'},
            { title: 'Insights', component: InsightsPage, icon: 'trending-up' },
            { title: 'Settings', component: SettingsPage, icon: 'cog' },
        ];
        // footer
        this.footer = [
            { title: 'About', component: AboutPage, icon: 'information-circle' },
        ]
        // display tutorial page once
        this.local = new Storage(LocalStorage);
        this.local.get('introShown')
            .then((result) => {
                if(result) {
                    // this.rootPage = TabsPage;
                    // this.rootPage = AddcategoryPage;
                    // this.rootPage = CategoryPage;
                    this.rootPage = TabsPage;
                    // this.rootPage = InsightsPage;
                    // this.rootPage = OverviewPage;
                    // this.rootPage = AboutPage;
                    // this.rootPage = TutorialPage;
                } else {
                    this.local.set('introShown', true);
                    this.rootPage = TutorialPage;
                }
            })
    }

    showFeedback() {
        let feedback = Modal.create(FeedbackModal);
        this.menu.close();
        this.nav.present(feedback);
    }

    initializeApp() {
        this.platform.ready().then(() => {
            StatusBar.styleDefault();
        });

    }

    openPage(page) {
        this.menu.close()
        this.nav.setRoot(page.component);
    }
}
