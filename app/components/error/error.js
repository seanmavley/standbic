import {Directive} from '@angular/core';

/*
  Generated class for the Error directive.

  See https://angular.io/docs/ts/latest/api/core/DirectiveMetadata-class.html
  for more info on Angular 2 Directives.
*/
@Directive({
  selector: '[error]' // Attribute selector
})
export class Error {
  constructor() {
    console.log('Hello World');
  }
}
