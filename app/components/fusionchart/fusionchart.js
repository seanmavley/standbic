import { Component, Input } from '@angular/core';

@Component({
  selector: 'fusionchart',
  templateUrl: 'build/components/fusionchart/fusionchart.html'
})
export class Fusionchart {
  constructor() {
    this.runFusioning();
  }

  runFusioning() {
    FusionCharts.ready(function() {
      var revenueChart = new FusionCharts({
        "type": "line",
        "renderAt": "chartContainer",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
            "caption": "Expenditure over period",
            "subCaption": "Last week",
            "xAxisName": "Past week",
            "yAxisName": "Amount spent",
            "lineThickness": "2",
            "paletteColors": "#0075c2",
            "baseFontColor": "#333333",
            "baseFont": "Helvetica Neue,Arial",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "showBorder": "0",
            "bgColor": "#ffffff",
            "showShadow": "0",
            "canvasBgColor": "#ffffff",
            "canvasBorderAlpha": "0",
            "divlineAlpha": "100",
            "divlineColor": "#999999",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "divLineGapLen": "1",
            "showXAxisLine": "1",
            "xAxisLineThickness": "1",
            "xAxisLineColor": "#999999",
            "showAlternateHGridColor": "0"
          },
          "data": [{
            "label": "Mon",
            "value": "200"
          }, {
            "label": "Tue",
            "value": "150"
          }, {
            "label": "Wed",
            "value": "100"
          }, {
            "label": "Thu",
            "value": "400"
          }, {
            "label": "Fri",
            "value": "250"
          }, {
            "label": "Sat",
            "value": "320"
          }, {
            "label": "Sun",
            "value": "50"
          }],
          "trendlines": [{
            "line": [{
              "startvalue": "18500",
              "color": "#1aaf5d",
              "displayvalue": "Average{br}weekly{br}footfall",
              "valueOnRight": "1",
              "thickness": "2"
            }]
          }]
        }

      });
      revenueChart.render();
    })
  }
}
