import { Page, NavController, Platform, IonicApp, Toast, NavParams } from 'ionic-angular';
import { MonthPage } from '../month/month';
import { PastPage } from '../past/past';
import { AddcategoryPage } from '../addcategory/addcategory';

// Services
import { CategoryService } from '../../providers/category-service/category-service';
import { TransactionService } from '../../providers/transaction-service/transaction-service';

@Page({
  templateUrl: 'build/pages/addtrans/addtrans.html',
})
export class AddtransPage {
  static get parameters() {
    return [
      [NavController],
      [CategoryService],
      [TransactionService],
      [Platform],
      [IonicApp],
      [NavParams]
    ];
  }

  constructor(nav, catservice, transervice, platform, app, navParams) {
    this.nav = nav;
    this.platform = platform;
    this.app = app;
    this.service = catservice;
    this.transact = transervice;
    this.addtrans = {};
    this.category = [];
    this.param = navParams;
    this.trans = {
      'category': ''
    };
    this.loadCategory();
  }

  loadCategory() {
    this.platform.ready().then(() => {
      this.service.getCategory()
        .then(data => {
          this.category = [];
          if (data.res.rows.length > 0) {
            for (var i = 0; i < data.res.rows.length; i++) {
              let item = data.res.rows.item(i);
              this.category.push({
                'id': item.id,
                'name': item.name,
                'type': item.type
              });
              // console.log(this.category);
            }
          }
          // this.category = Array.from(data.res.rows);
        }, error => {
          console.log('Error', error.err)
        })
    });
  }

  onPageDidEnter() {
    this.loadCategory();
  }

  displayToast() { // shall we?
    let toast = Toast.create({
      message: 'Successful',
      duration: 5000
    })

    this.nav.present(toast);
  }

  onSubmit(formData) {
    if (formData.valid) {
      console.log('Form submission is ', formData.value);
      this.transact.saveTransaction(formData.value)
        .then(data => {
          this.displayToast();
          // return to past page if it fired
          // the add transaction page
          if (this.param.get('fromPast') === 'past') {
            this.nav.setRoot(PastPage);
          } else {
            this.nav.setRoot(MonthPage);
          }
        }, error => {
          console.log('Error -> ', error.err);
        })
    }
  }

  gotoAddcategory() {
    this.nav.push(AddcategoryPage);
  }
}
