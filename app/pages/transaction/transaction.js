import { Page, NavController, NavParams, Toast, ActionSheet } from 'ionic-angular';
import { EdittransactionPage } from '../edittransaction/edittransaction';
// Service
import { TransactionService } from '../../providers/transaction-service/transaction-service';

@Page({
  templateUrl: 'build/pages/transaction/transaction.html',
})
export class TransactionPage {
  static get parameters() {
    return [
      [NavController],
      [NavParams],
      [TransactionService]
    ];
  }

  constructor(nav, navParams, transervice) {
    this.nav = nav;
    this.params = navParams;
    this.service = transervice;
    this.transaction = this.params.get('obj');
  }

  displayToast() { // shall we?
    let toast = Toast.create({
      message: 'Item deleted!',
      duration: 7000
    })

    this.nav.present(toast);
  }

  presentActionSheet(id) {
    let actionSheet = ActionSheet.create({
      title: 'You wanna delete. Really?',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        handler: () => {
          let leaveSheet = actionSheet.dismiss();
          console.log('This will delete', id);
          this.service.deleteTransaction(id)
            .then(data => {
              leaveSheet.then(() => {
                this.nav.pop();
                this.displayToast();
                console.log('Successful ->', data.res)
              })
            }, error => {
              console.log('Error ->', data.err);
            });
          return false;
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelled');
        }
      }]
    });
    this.nav.present(actionSheet);
  }

  editTransaction(obj) {
    this.nav.push(EdittransactionPage, {
      'obj': obj
    });
  }

  deleteTransaction(id) {
    this.service.deleteTransaction(id);
    this.nav.pop();
  }
}
