import { Page, NavController, Platform, IonicApp } from 'ionic-angular';
import { TransactionPage } from '../transaction/transaction';
import { AddtransPage } from '../addtrans/addtrans';
import moment from 'moment';

// Services
import { CategoryService } from '../../providers/category-service/category-service';
import { TransactionService } from '../../providers/transaction-service/transaction-service';

@Page({
  templateUrl: 'build/pages/past/past.html',
})

export class PastPage {
  static get parameters() {
    return [
      [NavController],
      [TransactionService],
      [Platform]
    ];
  }

  constructor(nav, transervice, platform) {
    this.nav = nav;
    this.platform = platform;
    this.service = transervice;
    this.transactions = [];
    // dates
    this.start = moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD');
    this.end = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD');
    this.month = moment().subtract(1, 'month').format('MMMM YYYY');
    this.loadPastMonth();

    // this.total = null;
    // this.totalTrans();
  }


  totalTrans() {
    this.platform.ready()
      .then(() => {
        this.service.sumAmountPeriod(this.start, this.end)
          .then(data => {
            console.log(data.res.rows[0]["sum(amount)"]);
            this.total = data.res.rows[0]["sum(amount)"];
          }, error => {
            console.log('Error', error.err);
          })
      })
  }

  loadPastMonth() {
    // get for this month, using same function
    // from service
    this.platform.ready()
      .then(() => {
        // 1st day of last month
        console.log('Start date: ' + this.start + ' ' + 'End date: ' + this.end);
        this.service.getPeriod(this.start, this.end)
          .then(data => {
            this.transactions = [];
            if (data.res.rows.length > 0) {
              for (var i = 0; i < data.res.rows.length; i++) {
                let item = data.res.rows.item(i);
                this.transactions.push({
                  'id': item.id,
                  'amount': item.amount,
                  'category': item.category,
                  'note': item.note,
                  'createdAt': item.createdAt
                })
              }
              console.log(this.transactions);
            }
          })
      })
  }

  onPageDidEnter() {
    this.loadPastMonth();
  }

  addTransaction() {
    this.nav.push(AddtransPage, {
      'fromPast': 'past'
    });
  }

  goDetail(obj) {
    this.nav.push(TransactionPage, {
      'obj': obj
    })
  }

}
