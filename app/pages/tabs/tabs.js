import {Page} from 'ionic-angular';
import {MonthPage} from '../month/month';
import {PastPage} from '../past/past';
import {FamilyPage} from '../family/family';


@Page({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {
  constructor() {
    this.MonthPage = MonthPage;
    this.PastPage = PastPage;
    this.FamilyPage = FamilyPage;
  }
}