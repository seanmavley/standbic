import { Page, NavController, Platform, IonicApp, Toast, ActionSheet } from 'ionic-angular';
import { AddcategoryPage } from '../addcategory/addcategory';
import { CategorydetailPage } from '../categorydetail/categorydetail';
import { EditcategoryPage } from '../editcategory/editcategory';

// Services
import { CategoryService } from '../../providers/category-service/category-service';

@Page({
  templateUrl: 'build/pages/category/category.html',
})

export class CategoryPage {
  static get parameters() {
    return [
      [NavController],
      [Platform],
      [CategoryService],
      [IonicApp]
    ];
  }

  constructor(nav, platform, service, app) {
    this.nav = nav;
    this.app = app;
    this.platform = platform;
    this.service = service;
    this.category = [];
    this.loadCategory();
  }

  // clears database
  killSwitch() {
    this.service.deleteTable()
      .then(data => {
        console.log('Success deleted table', data.res);
      }, error => {
        console.log('Error ', error.err)
      })
  }

  loadCategory() {
    this.platform.ready().then(() => {
      this.service.getCategory()
        .then(data => {
          this.category = [];
          if (data.res.rows.length > 0) {
            for (var i = 0; i < data.res.rows.length; i++) {
              let item = data.res.rows.item(i);
              this.category.push({
                'id': item.id,
                'name': item.name,
                'type': item.type
              });
              console.log(this.category);
            }
          }
          // this.category = Array.from(data.res.rows);
        }, error => {
          console.log('Error', error.err)
        })
    });
  }

  onPageDidEnter() {
    this.loadCategory();
  }

  gotoAdd() {
    this.nav.push(AddcategoryPage);
  }

  gotoCategoryDetail(obj) {
    this.nav.push(CategorydetailPage, {
      'obj': obj
    });
  }

  editCategory(obj) {
    this.nav.push(EditcategoryPage, {
      'obj': obj
    });
  }

  displayToast() { // shall we?
    let toast = Toast.create({
      message: 'Item deleted!',
      duration: 5000
    })

    this.nav.present(toast);
  }

  editCategory(item) {

  }

  deleteCategory(id) {
    let actionSheet = ActionSheet.create({
      title: 'You wanna delete. Really?',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        handler: () => {
          let leaveSheet = actionSheet.dismiss();
          console.log('This will delete', id);
          this.service.deleteCategory(id)
            .then(data => {
              leaveSheet.then(() => {
                this.loadCategory();
                this.displayToast();
                console.log('Successful ->', data.res)
              })
            }, error => {
              console.log('Error ->', data.err);
            });
          return false;
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelled');
        }
      }]
    });
    this.nav.present(actionSheet);
  }
}
