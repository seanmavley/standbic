import {Page, NavController} from 'ionic-angular';

@Page({
  templateUrl: 'build/pages/family/family.html',
})
export class FamilyPage {
  static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
  }
}
