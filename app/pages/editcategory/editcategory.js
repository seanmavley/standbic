import { Page, NavController, NavParams } from 'ionic-angular';
import { FORM_DIRECTIVES } from '@angular/common';
// Services
import { CategoryService } from '../../providers/category-service/category-service';

@Page({
  directives: [FORM_DIRECTIVES],
  templateUrl: 'build/pages/editcategory/editcategory.html',
})
export class EditcategoryPage {
  static get parameters() {
    return [
      [NavController],
      [NavParams],
      [CategoryService]
    ];
  }

  constructor(nav, navparams, catservice) {
    this.nav = nav;
    this.params = navparams;
    this.service = catservice;
    this.category = {
      'id': this.params.get('obj').id,
      'name': this.params.get('obj').name,
      'type': this.params.get('obj').type
    }
  }

  onSubmit(formData) {
    if (formData.value) {
      console.log(formData);
      this.service.updateCategory(formData);
      this.nav.pop();
    }
  }
}
