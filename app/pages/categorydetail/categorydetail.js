import { Page, NavController, NavParams, ActionSheet, Toast } from 'ionic-angular';
import { AddcategoryPage } from '../addcategory/addcategory';
import { EditcategoryPage } from '../editcategory/editcategory';
import { CategoryPage } from '../categorydetail/categorydetail';
// Services
import { CategoryService } from '../../providers/category-service/category-service';

@Page({
  templateUrl: 'build/pages/categorydetail/categorydetail.html',
})
export class CategorydetailPage {
  static get parameters() {
    return [
      [NavController],
      [NavParams],
      [CategoryService]
    ];
  };

  constructor(nav, navParams, catservice) {
    this.nav = nav;
    this.service = catservice;
    this.params = navParams;
    console.log(this.params.get('obj'));
    this.category = this.params.get('obj');
  }

  editCategory(obj) {
    this.nav.push(EditcategoryPage, {
      'obj': obj
    });
  }

  displayToast() { // shall we?
    let toast = Toast.create({
      message: 'Item deleted!',
      duration: 7000
    })

    this.nav.present(toast);
  }

  presentActionSheet(id) {
    let actionSheet = ActionSheet.create({
      title: 'You wanna delete. Really?',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        handler: () => {
          let leaveSheet = actionSheet.dismiss();
          console.log('This will delete', id);
          this.service.deleteCategory(id)
            .then(data => {
              leaveSheet.then(() => {
                this.nav.pop();
                this.displayToast();
                console.log('Successful ->', data.res)
              })
            }, error => {
              console.log('Error ->', data.err);
            });
            return false;
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelled');
        }
      }]
    });
    this.nav.present(actionSheet);
  }

}
