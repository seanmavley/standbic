import { Page, NavController, NavParams, Toast } from 'ionic-angular';
import { FORM_DIRECTIVES } from '@angular/common';
import { CategoryService } from '../../providers/category-service/category-service';
import { CategoryPage } from '../category/category';

@Page({
  templateUrl: 'build/pages/addcategory/addcategory.html',
  directives: [FORM_DIRECTIVES]
})

export class AddcategoryPage {
  static get parameters() {
    return [
      [NavController],
      [NavParams],
      [CategoryService]
    ];
  }

  constructor(nav, navParams, catservice) {
    this.nav = nav;
    this.catservice = catservice;
    this.toEdit = navParams.get('name');
  }

  displayToast() { // shall we?
    let toast = Toast.create({
      message: 'Successful',
      duration: 5000
    })

    this.nav.present(toast);
  }

  onSubmit(formData) {
    if (formData.valid) {
      console.log('Form submission is ', formData.value);
      this.catservice.saveCategory(formData.value)
        .then((data) => {
          console.log('Success', data.res);
          this.displayToast();
          this.nav.setRoot(CategoryPage);
        }, (error) => {
          console.log('Error', error.err);
        });
    }
  }
}
