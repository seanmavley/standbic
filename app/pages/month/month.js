import { Page, NavController, IonicApp, Platform } from 'ionic-angular';
import { AddtransPage } from '../addtrans/addtrans'
import { TransactionPage } from '../transaction/transaction';
import { OverviewPage } from '../overview/overview';
import moment from 'moment';

// Services
import { CategoryService } from '../../providers/category-service/category-service';
import { TransactionService } from '../../providers/transaction-service/transaction-service';

@Page({
  templateUrl: 'build/pages/month/month.html',
})
export class MonthPage {
  static get parameters() {
    return [
      [NavController],
      [TransactionService],
      [Platform],
      [IonicApp]
    ];
  }

  constructor(nav, transervice, platform, app) {
    this.nav = nav;
    this.service = transervice;
    this.platform = platform;
    this.app = app;

    this.transactions = [];
    this.loadTransactions();
    // dates
    this.month = moment().format('MMMM YYYY');
    this.start = moment().startOf('month').format('YYYY-MM-DD'); // 1st of this month
    this.end = moment().format('YYYY-MM-DD'); // today's date
    // aggregations
    // this piece doesn't work
    // no idea why, at the moment
    // this.totalTrans();
  }

  totalTrans() {
    this.platform.ready()
      .then(() => {
        this.service.sumAmountPeriod(this.start, this.end)
          .then(data => {
            this.total = null;
            console.log(data.res.rows[0]["sum(amount)"]);
            this.total = data.res.rows[0]["sum(amount)"];
          }, error => {
            console.log('Error', error.err);
          })
      })
  }

  loadTransactions() {
    this.platform.ready()
      .then(() => {
        console.log(this.start + this.end);
        this.service.getPeriod(this.start, this.end)
          .then(data => {
            this.transactions = [];
            if (data.res.rows.length > 0) {
              for (var i = 0; i < data.res.rows.length; i++) {
                let item = data.res.rows.item(i);
                this.transactions.push({
                  'id': item.id,
                  'amount': item.amount,
                  'category': item.category,
                  'note': item.note,
                  'createdAt': item.createdAt
                })
              }
              console.log(this.transactions);
            }
          })
      })
  }

  addTransaction() {
    this.nav.push(AddtransPage);
  }

  goDetail(obj) {
    this.nav.push(TransactionPage, {
      'obj': obj,
    });
  }

  gotoOverview() {
    this.nav.push(OverviewPage);
  }

  onPageDidEnter() {
    this.loadTransactions();
    // this.totalTrans();
  }
}
