import { Page, NavController, Platform, IonicApp } from 'ionic-angular';
import { TransactionPage } from '../transaction/transaction';
import { AddtransPage } from '../addtrans/addtrans';
import moment from 'moment';

// Services
import { CategoryService } from '../../providers/category-service/category-service';
import { TransactionService } from '../../providers/transaction-service/transaction-service';

@Page({
  templateUrl: 'build/pages/overview/overview.html',
})
export class OverviewPage {
  static get parameters() {
    return [
      [NavController],
      [TransactionService],
      [Platform]
    ];
  }

  constructor(nav, transervice, platform) {
    this.nav = nav;
    this.platform = platform;
    this.service = transervice;
    this.transactions = [];
    this.loadThisMonth();
  }

  loadThisMonth() {
    // get for this month, using same function
    // from service
    this.platform.ready()
      .then(() => {
        var start = moment().startOf('month').format('YYYY-MM-DD');
        var end = moment().format('YYYY-MM-DD');
        console.log(start + end);
        this.service.getPeriod(start, end)
          .then(data => {
            this.transactions = [];
            if (data.res.rows.length > 0) {
              for (var i = 0; i < data.res.rows.length; i++) {
                let item = data.res.rows.item(i);
                this.transactions.push({
                  'id': item.id,
                  'amount': item.amount,
                  'category': item.category,
                  'note': item.note,
                  'createdAt': item.createdAt
                })
              }
              console.log(this.transactions);
            }
          })
      })
  }

  onPageDidEnter() {
    this.loadThisMonth();
  }

  addTransaction() {
    this.nav.push(AddtransPage);
  }

  goDetail(obj) {
    this.nav.push(TransactionPage, {
      'obj': obj
    })
  }

}
