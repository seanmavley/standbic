import {Page, NavController, View, Modal} from 'ionic-angular';
import { GraphPage } from '../graph/graph'
import { TransactionService } from '../../providers/transaction-service/transaction-service';
import { Fusionchart } from '../../components/fusionchart/fusionchart';

@Page({
  templateUrl: 'build/pages/insights/insights.html',
  directives: [Fusionchart]
})

export class InsightsPage {
  static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
  }

  showModal() {
    let modal = Modal.create(GraphPage);
    this.nav.present(modal);
  }
}
