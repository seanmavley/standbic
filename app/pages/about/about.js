import { Page, NavController, Modal, MenuController } from 'ionic-angular';
import { TutorialPage } from '../tutorial/tutorial';
import { FeedbackModal } from '../feedback/feedback';

@Page({
  templateUrl: 'build/pages/about/about.html'
})
export class AboutPage {
  static get parameters() {
    return [
      [NavController],
      [MenuController]
    ]
  }

  constructor(nav, menu) {
    this.nav = nav;
    this.menu = menu;
  }

  gotoTour() {
    console.log('I was clicked');
    this.nav.push(TutorialPage);
  }

  showFeedback() {
    // let nav = this.app.getComponent('nav');
    let feedback = Modal.create(FeedbackModal);
    this.menu.close();
    this.nav.present(feedback);
  }
}
