import {Page, NavController, MenuController} from 'ionic-angular';
import { MonthPage } from '../month/month';
import { TabsPage } from '../tabs/tabs';

@Page({
  templateUrl: 'build/pages/tutorial/tutorial.html'
})
export class TutorialPage {
  static get parameters() {
    return [[NavController], [MenuController]];
  }

  constructor(nav, menu) {
    this.nav = nav;
    this.menu = menu;
    this.showSkip = true;

    this.slides = [
      {
        title: "Welcome to <b>StanPiggy</b>",
        description: "I am an app made simple, to help manage you and your family's finances. It is free, fast and easy to use!",
        image: "img/first_image.png",
      },
      {
        title: "Manage finances lika'boss!",
        description: "Put your daily transaction into categories. Get insights into what you spend on what on which dates. Each month, learn how you fare and how much you carry ahead.",
        image: "img/second_image.png",
      },
      {
        title: "Together, as a Family.",
        description: "Invite your family, and work together. Do monitor each other into reaching goals set. <br> Be each other's keeper and grow a strong family finances, together!",
        image: "img/third_image.png",
      },
      {
        title: "One Account, Across all Devices",
        description: "Synced across all devices. You know what you enter here is there instantly. Add transactions exactly when it happens, and see updates beamed to all your devices instantly",
        image: "img/fourth_image.png",
      }
    ];
  }

  startApp() {
    this.nav.setRoot(TabsPage);
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

  onPageDidEnter() {
    // the left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  onPageDidLeave() {
    // enable the left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
