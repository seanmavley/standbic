import { Page, Modal, Toast, NavController, ViewController, MenuController } from 'ionic-angular';

@Page({
  templateUrl: 'build/pages/feedback/feedback.html',
})
export class FeedbackModal {
  static get parameters() {
    return [
      [NavController],
      [ViewController],
      [MenuController]
    ];
  }

  constructor(nav, view, menu) {
    this.nav = nav;
    this.menu = menu;
    this.view = view;
  }

  onSubmit(formData) {
    if(formData.valid) {
      console.log(formData);
      this.displayToast();
      this.view.dismiss();
    }
  }

  displayToast() { // shall we?
    let toast = Toast.create({
      message: 'Thank you! Feedback sent',
      duration: 5000
    })

    this.nav.present(toast);
  }

  close() {
    console.log("I was fired");
    this.view.dismiss();
  }
}
