import { Page, ViewController, NavController, Modal } from 'ionic-angular';
import { Fusionchart } from '../../components/fusionchart/fusionchart';

@Page({
  templateUrl: 'build/pages/graph/graph.html',
  directives: [Fusionchart]
})
export class GraphPage {
  static get parameters() {
    return [
      [NavController],
      [ViewController]
    ];
  }

  constructor(nav, viewCtrl) {
    this.nav = nav;
    this.viewCtrl = viewCtrl;
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
