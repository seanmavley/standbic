import { Page, NavController, NavParams } from 'ionic-angular';
import { MonthPage } from '../month/month';

// Service
import { TransactionService } from '../../providers/transaction-service/transaction-service';

@Page({
  templateUrl: 'build/pages/edittransaction/edittransaction.html',
})
export class EdittransactionPage {
  static get parameters() {
    return [
      [NavController],
      [NavParams],
      [TransactionService]
    ];
  }

  constructor(nav, navParams, transervice) {
    this.nav = nav;
    this.params = navParams;
    this.service = transervice;
    this.transaction = {
      'id': this.params.get('obj').id,
      'amount': this.params.get('obj').amount,
      'category': this.params.get('obj').category,
      'note': this.params.get('obj').note,
      'createdAt': this.params.get('obj').createdAt
    }
  }

  onSubmit(formData) {
    console.log(formData);
    this.service.updateTransaction(formData)
    this.nav.pop();
  }
}
