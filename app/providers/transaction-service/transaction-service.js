import {Injectable} from '@angular/core';
import { Storage, SqlStorage } from 'ionic-angular';

@Injectable()
export class TransactionService {
  static get parameters(){
    return []
  }  

  constructor() {
    this.storage = new Storage(SqlStorage);
    this.storage.query('CREATE TABLE IF NOT EXISTS trans (id INTEGER PRIMARY KEY AUTOINCREMENT, amount INTEGER, category TEXT, note TEXT, createdAt TEXT)');
  }

  getTransaction() {
    return this.storage.query('SELECT id, amount, category, note, createdAt FROM trans');
  }

  getPeriod(start, end) {
    return this.storage.query('SELECT id, amount, category, note, createdAt FROM trans WHERE createdAt BETWEEN ? AND ?', [start, end])
  }

  saveTransaction(data) {
    let sql = 'INSERT INTO trans (amount, category, note, createdAt) VALUES (?, ?, ?, ?)';
    return this.storage.query(sql, [data.amount, data.category, data.note, data.createdAt]);
  }

  updateTransaction(obj) {
    let sql = 'UPDATE trans SET id = ?, amount = ?, category = ?, note = ?, createdAt = ? WHERE id = ?';
    return this.storage.query(sql, [obj.id, obj.amount, obj.category, obj.note,  obj.createdAt, obj.id]);
  }

  deleteTransaction(id) {
    let sql = 'DELETE FROM trans WHERE id = (?)';
    return this.storage.query(sql, [id]);
  }

  deleteTable() {
    let sql = 'DELETE FROM trans';
    return this.storage.query(sql);
  }

  // some quick aggregations
  sumAmountPeriod(start, end) {
    let sql = 'SELECT sum(amount) FROM trans WHERE createdAt BETWEEN ? AND ?';
    return this.storage.query(sql, [start, end]);
  }

}

