import {Injectable} from '@angular/core';
import { Storage, SqlStorage } from 'ionic-angular';

@Injectable()
export class CategoryService {
  static get parameters(){
    return []
  }  

  constructor() {
    this.storage = new Storage(SqlStorage);
    this.storage.query('CREATE TABLE IF NOT EXISTS category (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, type TEXT)');
  }

  getCategory() {
    return this.storage.query('SELECT id, name, type FROM category');
  }

  saveCategory(data) {
    let sql = 'INSERT INTO category (name, type) VALUES (?, ?)';
    return this.storage.query(sql, [data.name, data.type]);
  }

  updateCategory(obj) {
    let sql = 'UPDATE category SET id = ?, name = ?, type = ? WHERE id = ?';
    return this.storage.query(sql, [obj.id, obj.name, obj.type, obj.id]);
  }

  deleteCategory(id) {
    let sql = 'DELETE FROM category WHERE id = (?)';
    return this.storage.query(sql, [id]);
  }

  deleteTable() {
    let sql = 'DELETE FROM category';
    return this.storage.query(sql);
  }

}

